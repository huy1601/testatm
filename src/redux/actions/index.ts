export enum Actions {
    LOG_IN = 'LOG_IN',
    LOG_OUT = 'LOG_OUT',
};

export const doLogin = (user: any) => {
    return {
        type: Actions.LOG_IN,
        payload: { user }
    };
};

export const doLogout = () => {
    return {
        type: Actions.LOG_OUT,
    };
};