import { combineReducers } from 'redux';
import authReducer from './auth/index';

export const rootReducer = combineReducers({
    authReducer: authReducer,
})

export type RootState = ReturnType<typeof rootReducer>