import { Actions } from '../../actions';


const initialState: any = {
    loggedIn: false,
    user: {},
};

export type ReduxAction = {
    type: Actions,
    payload?: any,
}

const authReducer = (state = initialState, action: ReduxAction) => {
    // console.log('state: ', action);
    switch (action.type) {
        case Actions.LOG_IN:
            console.log('action: ', action);
            return {
                ...state,
                loggedIn: true,
                user: {
                    account: action.payload.account,
                    password: action.payload.password,
                    token: action.payload.user.PRIVATE_TOKEN
                },
            };
        case Actions.LOG_OUT:
            console.log('state1: ', state);
            return initialState;
        default:
            return initialState;
    };
};

export default authReducer;