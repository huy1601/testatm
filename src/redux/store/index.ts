import AsyncStorage from '@react-native-community/async-storage';
import { combineReducers, createStore } from 'redux';
import { rootReducer } from '../reducers';
import { persistStore, persistReducer } from 'redux-persist';
// import storage from 'redux-persist/lib/storage';

const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
};

const persistedReducer: any = persistReducer(persistConfig, rootReducer);

// const configureStore = () => {
//     return createStore(rootReducer);
// };

const store = createStore(persistedReducer);
const persistor = persistStore(store);

export { store, persistor };