import { StyleSheet, } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#003f5c',
        alignItems: 'center',
        justifyContent: 'center',
    },
    alignCenter: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    rowSpaceBetween: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    rowAlignSpaceBetween: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    rowAlignCenter: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    row: {
        flexDirection: 'row'
    },
    rowAlign: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    top15: {
        marginTop: 15
    },
    top5: {
        marginTop: 5
    },
    bottom15: {
        marginBottom: 15
    },
    right5: {
        marginRight: 5
    },
    right10: {
        marginRight: 10
    },
    left5: {
        marginLeft: 5
    },
});

export default styles;