const ASSETS = {
    atmIcon: require('../assets/images/iconsAtm.png'),
    bgrProfile: require('../assets/images/bgrProfile.jpg'),
    avatar: require('../assets/images/avatar.png'),

};

export default ASSETS;