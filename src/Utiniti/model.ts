import { Actions } from '../redux/actions';

export type ReduxAction = {
    type: Actions,
    payload?: any
};