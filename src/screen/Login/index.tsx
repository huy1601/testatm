import React, { useEffect, useState } from 'react';
import {
    View,
    SafeAreaView,
    TextInput,
    TouchableOpacity,
    Text,
    Alert,
} from 'react-native';
import style from './style';
import EntyIcon from 'react-native-vector-icons/Entypo';
import { NaviProps } from '../../Model/NaviProps';
import styles from '../../themes/styles';
import { Screens } from '../../routers/Screens';
import asyncStorageHelpers, { StorageKey } from '../../api/Helper';
import { useSelector, useDispatch } from 'react-redux';
import { doLogin } from '../../redux/actions';
import BcryptReactNative from 'bcrypt-react-native';
import axios from 'axios';

interface Props extends NaviProps {

}

const user = {
    email: 'jase@gmail.com',
    password: "123456"
}
function Login(props: Props) {
    const [email, setEmail] = useState('jase@gmail.com');
    const [password, setPassword] = useState('123456');
    const [hidePassword, setHidePassword] = useState(true);
    const [loading, setLoading] = useState(true);

    const dispatch = useDispatch();

    const setPasswordVisibility = () => {
        setHidePassword(!hidePassword);
    };

    const onSaveLoginSession = (user: any) => {
        asyncStorageHelpers.saveObject(StorageKey.LOGIN_SESSION, user)
    }


    const onLogin = async () => {
        await fetch('http://192.168.1.47:8091/api/v1/auth/login', {
            method: "POST",
            headers: {
                Accept: "application/json",
                "content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
            .then((response) => response.json())
            .then(json => {
                console.log(json.PRIVATE_TOKEN);
                onSaveLoginSession(json.PRIVATE_TOKEN);
                if (json.message === 'Login successfully !') {
                    dispatch(doLogin(json));
                    // props.navigation.navigate(Screens.TabNavigation);
                } else {
                    Alert.alert('Sai thông tin đăng nhập, vui lòng kiểm tra lại!')
                }
            });
    }

    useEffect(() => {
        // onLogin();
    }, [])


    return (
        <SafeAreaView style={style.container}>
            <Text style={style.logo}>HeyAPP</Text>
            <View style={style.inputView}>
                <TextInput
                    style={style.inputText}
                    placeholder="Email..."
                    placeholderTextColor="#003f5c"
                    keyboardType="email-address"
                    value={email}
                    onChangeText={textInputEmail => setEmail(textInputEmail)}
                />
            </View>
            <View style={[style.inputView, styles.rowAlignSpaceBetween]}>
                <TextInput
                    style={style.inputText}
                    placeholder="Password..."
                    placeholderTextColor="#003f5c"
                    numberOfLines={1}
                    keyboardType="default"
                    value={password}
                    secureTextEntry={hidePassword}
                    onChangeText={textInputPassword => setPassword(textInputPassword)}
                />
                <TouchableOpacity
                    style={style.iconEye}
                    onPress={setPasswordVisibility}
                >
                    {hidePassword ? (
                        <EntyIcon name="eye-with-line" size={18} color={'white'} />
                    ) : (
                        <EntyIcon name="eye" size={18} color={'white'} />
                    )}
                </TouchableOpacity>
            </View>
            <TouchableOpacity>
                <Text style={style.forgot}>Forgot Password?</Text>
            </TouchableOpacity>
            <TouchableOpacity
                onPress={onLogin}
                style={style.loginBtn}
            >
                <Text style={style.loginText}>LOGIN</Text>
            </TouchableOpacity>
            <TouchableOpacity
                onPress={() => {
                    props.navigation.navigate(Screens.Singup);
                }}
            >
                <Text style={style.loginText}>Signup</Text>
            </TouchableOpacity>
        </SafeAreaView>
    )
}

export default Login;