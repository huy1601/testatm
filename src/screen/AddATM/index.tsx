import React, { useEffect, useState } from 'react';
import {
    View,
    SafeAreaView,
    TextInput,
    TouchableOpacity,
    Text,
    Alert,
} from 'react-native';
import style from './style';
import EntyIcon from 'react-native-vector-icons/Entypo';
import { NaviProps } from '../../Model/NaviProps';
import { Screens } from '../../routers/Screens';
import styles from '../../themes/styles';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../redux/reducers';

interface Props extends NaviProps {

}


function AddATM(props: Props) {

    const [nameATM, setNameATM] = useState('');

    const currentUser = useSelector((state: RootState) => state.authReducer.user);


    const onCreate = async () => {
        console.log('YOYO');
        try {
            const response = await fetch('http://192.168.1.47:8091/api/v1/atms', {
                method: "POST",
                headers: {
                    Authorization: `Bearer ${currentUser.token}`,
                    Accept: "application/json",
                    "content-Type": "application/json"
                },
                body: JSON.stringify({
                    name: nameATM,
                })
            });
            const json = await response.json();
            console.log('json: ', json);
            props.navigation.navigate(Screens.Atm);
        } catch (error) {
            console.error(error);
        }
    }

    useEffect(() => {
        // onRegister();
    }, []);


    return (
        <SafeAreaView style={style.container}>
            <Text style={style.logo}>Add ATM</Text>
            <View style={style.inputView}>
                <TextInput
                    style={style.inputText}
                    placeholder="name ATM"
                    placeholderTextColor="#003f5c"
                    keyboardType="email-address"
                    value={nameATM}
                    onChangeText={inputName => setNameATM(inputName)}
                />
            </View>
            <TouchableOpacity
                style={style.loginBtn}
                onPress={onCreate}
            >
                <Text style={style.loginText}>ADD</Text>
            </TouchableOpacity>
        </SafeAreaView>
    )
}

export default AddATM;