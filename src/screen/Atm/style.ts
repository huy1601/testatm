import { StyleSheet, } from 'react-native';

const style = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        paddingHorizontal: 10,
        paddingVertical: 15
    },
    width45: {
        width: '45%',
    },
    width55: {
        width: '55%',
    },
    addButton: {
        backgroundColor: '#2E64FE',
        borderRadius: 8,
        paddingVertical: 8,
        width: 100,
        alignItems: 'center',
        marginBottom: 10
    },
    addButtonText: {
        color: 'white',
        fontSize: 13,
    },
    listAtm: {
        width: '100%',
        // marginVertical: 40
    },
    atmItem: {
        borderWidth: 0.5,
        borderColor: 'black',
        borderRadius: 6,
        padding: 5,
        width: '47%',
        marginVertical: 6,
        marginHorizontal: 6
    },
    iconDelete: {
        color: 'red',
        alignSelf: 'flex-end',
        margin: 5
    },
    atmIcon: {
        width: 100,
        height: 100,
        resizeMode: 'contain',
        tintColor: '#2E9AFE',
        marginBottom: 10,
        marginTop: 15,

    },
    busy: {
        color: 'red',
        fontSize: 20,
        fontWeight: 'bold'
    },
    address: {
        fontSize: 16,
        color: 'black',
        marginTop: 5
    },
    atmName: {
        color: 'black',
        fontSize: 13,
        fontWeight: 'bold'
    },
    queueName: {
        color: 'black',
        fontSize: 13,
        fontWeight: 'normal'
    },
});

export default style;