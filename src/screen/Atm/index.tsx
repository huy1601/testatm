import React, { useEffect, useState } from 'react';
import {
    View,
    SafeAreaView,
    TextInput,
    TouchableOpacity,
    Text,
    Image,
    ScrollView,
    FlatList
} from 'react-native';
import style from './style';
import styles from '../../themes/styles';
import ASSETS from '../../assets/ASSETS';
import Icon from 'react-native-vector-icons/Entypo';
import AntIcon from 'react-native-vector-icons/AntDesign';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../redux/reducers';
import { NaviProps } from '../../Model/NaviProps';
import { Screens } from '../../routers/Screens';

interface Props extends NaviProps {

}

function Atm(props: NaviProps) {

    const [dataATM, setDataATM] = useState([]);

    const currentUser = useSelector((state: RootState) => state.authReducer.user);

    const onRemove = async (id: any) => {
        try {
            const response = await fetch(`http://192.168.1.47:8091/api/v1/atms/${id}`, {
                method: "DELETE",
                headers: {
                    Authorization: `Bearer ${currentUser.token}`,
                    Accept: "application/json",
                    "content-Type": "application/json"
                },
            });
            const json = await response.json();
            console.log('jsonDelete: ', json);
            // console.log('dataATM: ', dataATM);
        } catch (error) {
            console.error(error);
        }
    }

    const onRender = async () => {
        try {
            const response = await fetch('http://192.168.1.47:8091/api/v1/atms', {
                method: "GET",
                headers: {
                    Authorization: `Bearer ${currentUser.token}`,
                    Accept: "application/json",
                    "content-Type": "application/json"
                },
            });
            const json = await response.json();
            setDataATM(json.atm);
            // console.log('dataATM: ', dataATM);
        } catch (error) {
            console.error(error);
        }
    }

    useEffect(() => {
        onRender();
    }, [dataATM]);

    return (
        <SafeAreaView style={style.container}>
            {/* <ScrollView
                showsVerticalScrollIndicator={false}
            > */}
            <TouchableOpacity
                style={style.addButton}
                onPress={() => props.navigation.navigate(Screens.AddATM)}
            >
                <Text style={style.addButtonText}>Add new ATM</Text>
            </TouchableOpacity>
            <FlatList
                data={dataATM}
                style={style.listAtm}
                keyExtractor={(item: any) => item.id}
                numColumns={2}
                showsVerticalScrollIndicator={false}
                renderItem={({ item }) => {
                    return (
                        <View style={style.atmItem}>
                            <TouchableOpacity
                                onPress={() => onRemove(item.id)}
                            >
                                <AntIcon name='closecircle' size={18} style={style.iconDelete} />
                            </TouchableOpacity>
                            <View style={styles.alignCenter}>
                                <Image
                                    source={ASSETS.atmIcon}
                                    style={style.atmIcon}
                                />
                                <Text style={style.busy}>{item.status}</Text>
                                <Text style={style.address}>{item.client}</Text>
                            </View>
                            <View style={[styles.rowAlignCenter, styles.top15]}>
                                <Icon name='user' size={18} color={'black'} style={styles.right5} />
                                <View>
                                    <Text style={style.atmName}>{item.name}</Text>
                                    <Text style={style.queueName}>Pending transactions: {item.transaction}</Text>
                                </View>
                            </View>
                        </View>
                    )
                }}

            />
            {/* </ScrollView> */}
        </SafeAreaView>
    )
}

export default Atm;