import { StyleSheet, } from 'react-native';

const style = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        paddingHorizontal: 10,
        paddingVertical: 15
    },
    atmName: {
        color: 'black',
        fontSize: 13,
        fontWeight: 'bold'
    },
    queue: {
        fontSize: 20,
        color: 'black',
        fontWeight: 'bold',
        marginBottom: 15
    },
    queueView: {
        height: 400,
        backgroundColor: '#F5F5F5',
        paddingHorizontal: 5,
        paddingVertical: 3,
        borderRadius: 6,
        borderWidth: 0.5,
        borderColor: '#DCDCDC',
    },
    queueItems: {
        backgroundColor: 'white',
        borderRadius: 6,
        padding: 10,
        marginVertical: 5,
    },
    queueName: {
        color: 'black',
        fontSize: 13,
        fontWeight: 'normal'
    },
    process: {
        fontSize: 18,
        color: 'black'
    },
    titleItemsProcess: {
        fontSize: 13,
        color: 'black',
        fontWeight: 'bold'
    },
});

export default style;