import React, { useEffect, useState } from 'react';
import {
    View,
    SafeAreaView,
    TextInput,
    TouchableOpacity,
    Text,
    Image,
    ScrollView,
    FlatList
} from 'react-native';
import style from './style';
import styles from '../../themes/styles';
import ASSETS from '../../assets/ASSETS';
import Icon from 'react-native-vector-icons/Entypo';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../redux/reducers';


function Queue() {

    const [dataUser, setDataUser] = useState([]);
    const [dataClient, setDataClient] = useState([]);


    const currentUser = useSelector((state: RootState) => state.authReducer.user);

    const onRender = async () => {
        try {
            const response = await fetch('http://192.168.1.47:8091/api/v1/atms', {
                method: "GET",
                headers: {
                    Authorization: `Bearer ${currentUser.token}`,
                    Accept: "application/json",
                    "content-Type": "application/json"
                },
            });
            const json = await response.json();
            setDataUser(json.queue);
            setDataClient(json.processedClient);
            console.log('dataUser: ', dataUser);
            console.log('dataClient: ', dataClient);
        } catch (error) {
            console.error(error);
        }
    }

    useEffect(() => {
        onRender();
    }, [])


    return (
        <SafeAreaView style={style.container}>
            {/* <ScrollView
                showsVerticalScrollIndicator={false}
            > */}

            <View>
                <Text style={style.queue}>Queue</Text>
                {/* <View style={style.queueView}>
                    {B.map((key) => {
                        return (
                            <View
                                key={key}
                                style={[style.queueItems, styles.rowAlign]}
                            >
                                <Icon name='user' size={18} color={'black'} style={styles.right10} />
                                <View>
                                    <Text style={style.queueName}>Name: Olivia Bayer</Text>
                                    <Text style={style.queueName}>Transactions: 6</Text>
                                </View>
                            </View>
                        )
                    })}
                </View> */}
                <FlatList
                    data={dataUser}
                    style={style.queueView}
                    keyExtractor={(item: any, key: any) => `${key}`}
                    showsVerticalScrollIndicator={false}
                    renderItem={({ item }) => {
                        return (
                            <View
                                style={[style.queueItems, styles.rowAlign]}
                            >
                                <Icon name='user' size={18} color={'black'} style={styles.right10} />
                                <View>
                                    <Text style={style.queueName}>Name: {item.name}</Text>
                                    <Text style={style.queueName}>Transactions: {item.transaction}</Text>
                                </View>
                            </View>
                        )
                    }}
                />
            </View>
            <View style={styles.top15}>
                <Text style={[style.process, styles.bottom15]}>Processed client</Text>
                <Text style={style.atmName}>Idaho: <Text style={style.queueName}>Marsha Hill, Dana Purdy</Text></Text>
                <Text style={[style.atmName, styles.top5]}>West virginia: <Text style={style.queueName}>Larry Nienow DDS, Ms. Edith Schmeler</Text></Text>
                <Text style={[style.atmName, styles.top5]}>Vermont: <Text style={style.queueName}>Cedric Hoppe, Orlando O'Reily</Text></Text>
            </View>
            {/* </ScrollView> */}
        </SafeAreaView>
    )
}

export default Queue;