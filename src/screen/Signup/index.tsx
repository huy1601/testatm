import React, { useEffect, useState } from 'react';
import {
    View,
    SafeAreaView,
    TextInput,
    TouchableOpacity,
    Text,
    Alert,
} from 'react-native';
import style from './style';
import EntyIcon from 'react-native-vector-icons/Entypo';
import { NaviProps } from '../../Model/NaviProps';
import { Screens } from '../../routers/Screens';
import styles from '../../themes/styles';

interface Props extends NaviProps {

}


function Signup(props: Props) {
    const [email, setEmail] = useState('abc123@gmail.com');
    const [password, setPassword] = useState('123456');
    const [enterPassword, setEnterPassword] = useState('');
    const [hidePassword, setHidePassword] = useState(true);

    const setPasswordVisibility = () => {
        setHidePassword(!hidePassword);
    };

    const onRegister = async () => {
        console.log('YOYO');
        try {
            const response = await fetch('http://192.168.1.47:8091/api/v1/auth/register', {
                method: "POST",
                headers: {
                    Accept: "application/json",
                    "content-Type": "application/json"
                },
                body: JSON.stringify({
                    email: 'abc123@gmail.com',
                    password: '123456'
                })
            });
            const json = await response.json();
            console.log('json: ', json);
            if (json.message === 'Register successfully !') {
                props.navigation.navigate(Screens.Login)
            } if (json.message === 'email already exists !') {
                Alert.alert('Email đã tồn tại, đăng ký thất bại!')
            }
            else {
                Alert.alert('Đăng ký thất bại, vui lòng kiểm trai lại!');
            }
        } catch (error) {
            console.error(error);
        }
    }

    useEffect(() => {
        // onRegister();
    }, []);


    return (
        <SafeAreaView style={style.container}>
            <Text style={style.logo}>HeyAPP</Text>
            <View style={style.inputView}>
                <TextInput
                    style={style.inputText}
                    placeholder="Email..."
                    placeholderTextColor="#003f5c"
                    keyboardType="email-address"
                    value={email}
                    onChangeText={textInputEmail => setEmail(textInputEmail)}
                />
            </View>
            <View style={[style.inputView, styles.rowAlignSpaceBetween]}>
                <TextInput
                    style={style.inputText}
                    placeholder="Password..."
                    placeholderTextColor="#003f5c"
                    numberOfLines={1}
                    keyboardType="default"
                    value={password}
                    secureTextEntry={hidePassword}
                    onChangeText={textInputPassword => setPassword(textInputPassword)}
                />
                <TouchableOpacity
                    style={style.iconEye}
                    onPress={setPasswordVisibility}
                >
                    {hidePassword ? (
                        <EntyIcon name="eye-with-line" size={18} color={'white'} />
                    ) : (
                        <EntyIcon name="eye" size={18} color={'white'} />
                    )}
                </TouchableOpacity>
            </View>
            {/* <View style={style.inputView}>
                <TextInput
                    style={style.inputText}
                    placeholder="Enter the Password..."
                    placeholderTextColor="#003f5c"
                    keyboardType="default"
                    value={enterPassword}
                    onChangeText={textInputEnterPassword => setEnterPassword(textInputEnterPassword)}
                />
            </View> */}
            <TouchableOpacity
                style={style.loginBtn}
                onPress={onRegister}
            >
                <Text style={style.loginText}>SIGNUP</Text>
            </TouchableOpacity>
            <TouchableOpacity
                onPress={() => props.navigation.navigate(Screens.Login)}
            >
                <Text style={style.loginText}>Login</Text>
            </TouchableOpacity>
        </SafeAreaView>
    )
}

export default Signup;