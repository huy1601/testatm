import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Screens } from './Screens';
import Atm from '../screen/Atm';
import Profile from '../screen/Profile';
import Icon from 'react-native-vector-icons/AntDesign';
import EntyIcon from 'react-native-vector-icons/Entypo';
import FonttyIcon from 'react-native-vector-icons/FontAwesome';
import Queue from '../screen/Queue';

const Tab = createBottomTabNavigator();

export default function TabNavigation() {
    return (
        <Tab.Navigator
            initialRouteName={Screens.Atm}
            screenOptions={{
                tabBarActiveTintColor: '#fb5b5a',
                tabBarInactiveTintColor: 'white',
                headerShown: false,
                tabBarStyle: {
                    // paddingVertical: 7,
                    backgroundColor: '#003f5c'
                }
            }}
        >
            <Tab.Screen
                name={Screens.Atm}
                component={Atm}
                options={{
                    title: 'ATM',
                    tabBarIcon: ({ focused }) => focused ? (
                        <FonttyIcon name='credit-card' size={24} color={'#fb5b5a'} />
                    ) :
                        <FonttyIcon name='credit-card' size={24} color={'white'} />
                }}
            />
            <Tab.Screen
                name={Screens.Queue}
                component={Queue}
                options={{
                    title: 'Queue',
                    tabBarIcon: ({ focused }) => focused ? (
                        <EntyIcon name='list' size={24} color={'#fb5b5a'} />
                    ) :
                        <EntyIcon name='list' size={24} color={'white'} />
                }}
            />
            <Tab.Screen
                name={Screens.Profile}
                component={Profile}
                options={{
                    title: 'Profile',
                    tabBarIcon: ({ focused }) => focused ? (
                        <Icon name='user' size={24} color={'#fb5b5a'} />
                    ) :
                        <Icon name='user' size={24} color={'white'} />
                }}
            />
        </Tab.Navigator >
    )
}