export enum Screens {
    TabNavigation = 'TabNavigation',
    AuthNavigation = 'AuthNavigation',
    Singup = 'SingUp',
    Login = 'LogIn',
    Atm = 'Atm',
    Queue = 'Queue',
    Profile = 'Profile',
    AddATM = 'AddATM'
}